package com.jesiontrop.workhea.repository;

import com.jesiontrop.workhea.model.Employment;
import org.springframework.data.repository.CrudRepository;

public interface EmploymentRepository extends CrudRepository<Employment, Long> {
}
