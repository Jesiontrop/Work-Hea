package com.jesiontrop.workhea.repository;

import com.jesiontrop.workhea.model.Specialization;
import org.springframework.data.repository.CrudRepository;

public interface SpecializationRepository extends CrudRepository<Specialization, Long> {
}
