package com.jesiontrop.workhea.repository;

import com.jesiontrop.workhea.model.Schedule;
import org.springframework.data.repository.CrudRepository;

public interface ScheduleRepository extends CrudRepository<Schedule, Long> {
}
