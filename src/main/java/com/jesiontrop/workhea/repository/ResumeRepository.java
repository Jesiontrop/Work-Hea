package com.jesiontrop.workhea.repository;

import com.jesiontrop.workhea.model.Resume;
import org.springframework.data.repository.CrudRepository;

public interface ResumeRepository extends CrudRepository<Resume, Long> {
}
